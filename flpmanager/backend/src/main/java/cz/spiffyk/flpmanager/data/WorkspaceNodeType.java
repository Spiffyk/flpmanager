package cz.spiffyk.flpmanager.data;

public enum WorkspaceNodeType {
	SONG, PROJECT
}
