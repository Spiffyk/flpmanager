package cz.spiffyk.flpmanager.data;

public interface WorkspaceNode {
	/**
	 * Gets the type of the workspace node
	 * @return {@link WorkspaceNodeType} value
	 */
	public WorkspaceNodeType getType();
}
